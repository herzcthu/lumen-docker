FROM php:7.2-apache
COPY 000-default.conf /etc/apache2/sites-available/
COPY src/ /var/www/html/
WORKDIR /var/www/html/
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php && chmod +x composer.phar && mv composer.phar /usr/local/bin/composer && \
    php -r "unlink('composer-setup.php');"

RUN apt-get update && apt-get install -y \
        libzip-dev \
        zip \
  && docker-php-ext-configure zip --with-libzip \
  && docker-php-ext-install zip

RUN composer global require "laravel/lumen-installer"

RUN a2enmod rewrite

EXPOSE 80
